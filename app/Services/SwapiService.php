<?php

namespace App\Services;

use GuzzleHttp\Client;

class SwapiService implements SourceInterface
{
    const BASE_URL = 'https://swapi.co/api/';

    /**
     * Http Client.
     *
     * @var Client
     */
    protected $httpClient;

    /**
     * SwapiService constructor.
     *
     * @param \GuzzleHttp\Client $httpClient
     */
    public function __construct(Client $httpClient)
    {
        $this->httpClient = $httpClient;
    }

    /**
     * Get all people.
     *
     * @param int|null $page
     * @return array
     */
    public function getAll(int $page = null): array
    {
        $url = self::BASE_URL . 'people';
        if ($page) {
            $url .= '?' . http_build_query(['page' => $page]);
        }
        $response = $this->httpClient->get($url);
        $data = json_decode($response->getBody(), true);
        foreach ($data['results'] as &$item) {
            $url_parts = explode('/', rtrim($item['url'], '/'));
            $item['id'] = end($url_parts);
        }

        $query = parse_url($data['next'], PHP_URL_QUERY);
        parse_str($query, $pagination);
        $data['next'] = !empty($pagination['page']) ? $pagination['page'] : null;

        $query = parse_url($data['previous'], PHP_URL_QUERY);
        parse_str($query, $pagination);
        $data['previous'] = !empty($pagination['page']) ? $pagination['page'] : null;

        return $data;
    }

    /**
     * Get person details.
     *
     * @param $id
     *
     * @return array
     */
    public function get(int $id): array
    {
        $response = $this->httpClient->get(self::BASE_URL . 'people/' . $id);

        return json_decode($response->getBody(), true);
    }
}