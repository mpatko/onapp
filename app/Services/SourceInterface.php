<?php

namespace App\Services;

/**
 * Interface SourceInterface
 * @package App\Services
 */
interface SourceInterface
{
    /**
     * Get person details.
     *
     * @param $id
     *
     * @return array
     */
    public function get(int $id);

    /**
     * Get all people.
     *
     * @param int|null $page
     * @return array
     */
    public function getAll(int $page = null);
}