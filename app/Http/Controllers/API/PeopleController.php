<?php

namespace App\Http\Controllers\API;

use App\Services\SwapiService;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class PeopleController extends Controller
{

    /**
     * Swapi Service.
     *
     * @var \App\Services\SwapiService
     */
    protected $swapiService;

    /**
     * PeopleController constructor.
     *
     * @param \App\Services\SwapiService $swapiService
     */
    public function __construct(SwapiService $swapiService)
    {
        $this->swapiService = $swapiService;
    }


    /**
     * Get all people data.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $people = $this->swapiService->getAll($request->query('page'));

        return response()->json($people);
    }

    /**
     * Get specific person details.
     *
     * @param $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $person = $this->swapiService->get($id);

        return response()->json($person);
    }
}
