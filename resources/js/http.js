import axios from 'axios'
 import eventHub from './eventhub'


const instance = axios.create({
  baseURL: '/api'
})

instance.interceptors.request.use(config => {
  eventHub.$emit('request-start')
  return config
}, error => {
  eventHub.$emit('request-error')
  return Promise.reject(error)
})

instance.interceptors.response.use(response => {
  eventHub.$emit('response-success')
  return response
}, error => {
  eventHub.$emit('response-error')
  return Promise.reject(error)
})

export default instance