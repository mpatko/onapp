window.Vue = require('vue')
import VueRouter from 'vue-router'

window.Vue.use(VueRouter)

import Index from './components/Index'
import View from './components/View'
import App from './components/App'

const routes = [
  {
    path: '/',
    name: 'index',
    component: Index
  },
  {
    path: '/people/:id',
    name: 'view',
    component: View
  }
];
const router = new VueRouter({ routes })

new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})

